// class
// writing style
class NamaClass {
    // need to build class
    constructor(firstName, lastName) {
        // this is an object inside class
        this.firstName = firstName
        this.lastName = lastName
    }
    // method
    getFullName() {
        return `${this.firstName} ${this.lastName} `
    }
    async getAsync() {

    }
}

new Date() // take time when declaration, difference in nanoseconds

// way to use
const l = new NamaClass("L", "Lawliett")

// way to use property this in class
console.log(l.firstName);
console.log(l.lastName);
console.log(l.getFullName);
console.log(await l.getAsync);

const bigdaddy = {
    gold: true,
    certificate: true
}

const smallchild = {
    ...bigdaddy,
    hp: true
}
console.log(smallchild);

class Bigdaddy {
    constructor(gold, certificate) {
        this.gold = gold
        this.certificate = certificate
    }
}

const dadd = new Bigdaddy(false, false);
console.log(dadd.gold);
console.log(dadd.certificate);
console.log(dadd.hp);

class Smallchild extends Bigdaddy {
    constructor(hp, gold, certificate) {
        super(gold, certificate) // new Bigdaddy()
        this.hp = hp
    }
}

const childd = new Smallchild(false, true, true);
console.log(childd.gold);
console.log(childd.certificate);
console.log(childd.hp);