const user = {
    firstName: 'chicken',
    lastName: 'dinner'
}

const additionalInfo = {
    age: 2,
    anationality: "Indonesia"
}

const newUser = {
    ...user,
    ...additionalInfo
}

console.log(newUser);