## Initiate environment and database
1. `git init`
2. `npm init`
3. `npm install express dotenv pg sequelize`
4. create `.env` with contents of:
   ```env
   1. DB_USERNAME=""
   2. DB_PASSWORD=""
   3. DB_DATABASE=""
   4. DB_HOST=""
   5. DB_DIALECT=""
   6. PORT=
   ```
5. create `.gitignore` with:
   ```gitignore
   1. node_modules
   2. package-lock.json
   3. .env
   ```
6. `sequelize init`
7. rename `config.json` -> `config.js`
   1. use `dotenv` to use environment variable in sequelize config
8. change `model/index.js` config variable require to changed `config.js`
9.  `sequelize db:create`
10. `sequelize model:create --name Users --attributes username:string,password:string,fullName:string`
11. `sequelize model:create --name Articles --attributes content:text,userId:string`

## Create relation
1. in migration `user`, change:
   1. data type:
      1. id to `string(22)`
         1. delete `autoincrement`
      2. username to `string(18)`
      3. fullName to `string(52)`
2. in migration `articles`, change:
   1. data type
      1. id to `string(22)`
      2. userId to `string(22)`
         1. add references to model with key `id`
3. `sequelize db:migrate`
4. in model `articles`, add:
   1. relation `hasOne` to model `Users`
5. in model `users`, add:
   1. relation `hasMany` to model `Articles`

## Create API
1. create `index.js`
   1. use `dotenv`
   2. initiate express
   3. use `express.json()` middleware
2. create route folder & `authRoute.js` file
   1. `/login` with return of request body
   2. `/request` with return of request body
3. require `authRoute.js` to `index.js` & use it as middleware with route prefix
4. add script `start` and `dev` in `package.json`

## Set up mcr
1. create controller folder & `userController.js`
   1. `npm i bcrypt`
   2. add `JWT_SECRET` env variable
   3. make register and login method
2. `npm install jsonwebtoken`
3. `npm install nanoid`
4. use userController to register and login route in `authRoute.js`

## Set up authorization 
1. create `articleController.js` with crud
2. create `articleRoute.js` with crud
3. require articleRoute to `index.js` with /article prefix
4. `npm install passport passport-jwt`
5. create middleware folder & `passportMidleware.js`
   1. implement passport jwt middleware
6. use passport middleware to article routes
7. modify post and get article route to use user id in token

## Setup heroku

### First
1. `heroku login`
2. heroku create (name app for ex:) app-name *cant get the same as heroku
   1. `heroku create auth-api-abiyyuramadhan`
   2. after make app, check `git remote-v` and will be new remote named heroku
   3. `git remote -v` > if there are heroku
   4. `git remote add heroku`  `link` *can see in settings -> heroku git URL*
   5. 
### Second, check addon
3. `heroku info` + `--app app-name` > our heroku info
4.  `heroku addons:services`
5.  `heroku addons:services | grep postgres`
   
### Third, commands only for wsl add in the end with `--app app-name`
6. `heroku info` > our heroku info
7.  `heroku addons:create heroku-postgresql` > adds addon postgres
8.  `heroku addons --all` > list addon that done added
9.  `heroku config -s` > check environment variable
10. `heroku config:set JWT_SECRET = "cant access my secret" PGSSLMODE=no-verify` > set/add env var

### Setup push
11. `npm install -D sequelize-cli`
12. add `build` script in `package.json` with value `sequelize db:migrate`
13. edit `config.js` in `production` object to use `DATABASE_URL` environment variable  with logging: false -> 
   ```js
    1.  "use_env_variable": "DATABASE_URL",
    2.  "logging: false" 
   ```
14. make sure to commit all progress in git with `git commit -m "message"` before `git push`
15. `git push -f heroku master`
